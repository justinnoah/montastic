montastic
=========
Interpret Monte in Racket

Setup/Requirements:
===
- Racket (currently developing with v7.2)
- `raco pkg install beautiful-racket brag` (or, see info.rkt)
- `cd /path/to/montastic`
- `raco pkg install ./montastic`

Running Monte
===
For now, prefix monte source with:
```racket
#lang montastic
```

and run:
```shell
racket /path/to/**prefixed**/monte/source
```
