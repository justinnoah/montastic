#lang racket/base
(require montastic/lexer brag/support rackunit racket/list)

(define (lex str)
    (apply-port-proc montastic-lexer str))

(check-equal? (lex "") empty)

(check-equal? (lex "def")
              (list (srcloc-token (token 'def "def")
                                  (srcloc 'string 1 0 1 3))))

              
